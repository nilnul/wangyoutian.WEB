﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Assembly="nilnul._web_._CTR_" Namespace="nilnul._web_._CTR_.enCn_" TagPrefix="cc1" %>

<section>


<h1>
	<cc1:enCn__BySession ID="EnCn__BySession1" runat="server">
		<%--first one is english--%>
		<asp:View ID="View1" runat="server">MetA</asp:View>
		<asp:View ID="View2" runat="server">外星球与元宇宙</asp:View>
	</cc1:enCn__BySession>
</h1>

	<div>
		<cc1:enCn__BySession ID="EnCn__BySession2" runat="server">
			<%--first one is english--%>
			<asp:View ID="View3" runat="server">
				Use social engineering to build a better society if we set foot to extraterrestrial universe; mistakes on earth shall not be repeated.

			</asp:View>
			<asp:View ID="View4" runat="server">

				这是一款虚拟现实社会工程学游戏，在数字孪生世界里建立外星球社会，编写法典，建立机构，发行货币。它采用软件工程方法，确保系统里不要有重复的低级Bug。<br />
				这款游戏与普通电子游戏不同，它是严肃的需要思考的，它是建设性的，它是专业的，它让玩游戏的人两眼聚光而非散光。
			</asp:View>
		</cc1:enCn__BySession>
		<p>
			这款游戏和地球无关。不要在地球虚拟网络里喷国际形势了，你的韬略不应该就这样浪费掉，来这里建设一个新的数字星球吧，你的所有梦想都将变成，呃，让虚拟的归虚拟，现实的归现实吧。
		</p>
		<p>
			我们将择期把此款游戏的库推送到开源网站上，从而所有人都可以不同程度地参与编辑和搭建。
		</p>
		<p>
			敬请期待。
		</p>

		<%--<ol>
			<li>游戏就从城门立木开始吧
			</li>
		</ol>--%>

	</div>
</section>
