﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("wangyoutian.WEB")]
[assembly: AssemblyDescription("wangyoutian's public website")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("wangyoutian")]
[assembly: AssemblyProduct("wangyoutian.WEB")]
[assembly: AssemblyCopyright("wangyoutian")]
[assembly: AssemblyTrademark("wangyoutian.WEB")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d37e81d6-5410-4dfd-9c07-27cbe14d4db6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
