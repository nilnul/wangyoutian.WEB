﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Assembly="nilnul._web_._CTR_" Namespace="nilnul._web_._CTR_.enCn_" TagPrefix="cc1" %>
<%@ Register Src="Courses.ascx" TagPrefix="uc1" TagName="Courses" %>



<section>
	<h1>
		<cc1:enCn__BySession ID="EnCn__BySession1" runat="server">
			<%--first one is english--%>
			<asp:View ID="View1" runat="server">Teach</asp:View>
			<asp:View ID="View2" runat="server">教学</asp:View>
		</cc1:enCn__BySession>

	</h1>


	<p>
		网站开发全栈：数据库、C#程序编写、网页（Html+Css+Js）
	</p>
	<uc1:Courses runat="server" ID="Courses" />

</section>
