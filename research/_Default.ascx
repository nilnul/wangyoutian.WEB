﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<%@ Register Assembly="nilnul._web_._CTR_" Namespace="nilnul._web_._CTR_.enCn_" TagPrefix="cc1" %>
<%@ Register Src="~/_research/_intro_/heading/H1.ascx" TagPrefix="uc1" TagName="H1" %>


<%@ Register Src="~/research/_enterprise_/Com.ascx" TagPrefix="uc1" TagName="Com" %>




<%@ Register src="../prj_/nilnul/grow/Link.ascx" tagname="Link" tagprefix="uc2" %>




<section>
	<uc1:H1 runat="server" ID="H1" />



	<p>
		发表论文少量，著、译书各一。
	</p>
    <p>
		大量研究体现在代码中，正逐渐以nilnul代号在github等网站开源。
	</p>
	<p>
		产业化部分由<a href="http://nilnul.com"><% = nilnul.web.http.svr.app.module_.ling_.enCn_.BySessionX.GetEnCn("nilnul.com","零空网际") %></a>企业运营。
	    （<uc2:Link ID="Link1" runat="server" />
        ）</p>
    <p>
		请支持我们的研发和产业转化工作：</p>
	<uc1:Com runat="server" id="Com" />
</section>
