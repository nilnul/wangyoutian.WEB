﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="Categories.ascx" TagName="Categories" TagPrefix="uc1" %>

<h1>
	欢迎访问 王有天 的个人网站


</h1>
<address>
	<a href="http://nilnul.org/wangyoutian">nilnul.org/wangyoutian</a>
</address>
<address style="margin-top: 0px">
	教师、工程师、公民。

</address>
<hr />

<uc1:Categories ID="Categories1" runat="server" />


<hr />
<div>
	<p>
		您可以在如下网址参与编辑本网站：

	</p>
	<ul>
		<li>
			<a href="https://github.com/wangyoutian/wangyoutian.WEB">https://github.com/wangyoutian/wangyoutian.WEB</a></li>
		<li>备用网址： <a href="https://gitlab.com/wangyoutian/wangyoutian.web">https://gitlab.com/wangyoutian/wangyoutian.web</a>

		</li>
	</ul>
	<p>关于本网站有何问题，请发邮件给: <a href="mailto:wangyoutian@msn.com">wangyoutian@msn.com</a></p>
	<address>
		网站内容均为个人观点。保留所有版权。
	</address>

</div>
