﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/_org/_intro_/Link.ascx" TagPrefix="uc1" TagName="Link" %>




<section>
	<h1>
		<uc1:Link runat="server" ID="Link" />

	</h1>

	<p>
		我认为计算机语言是承载、传递科技知识的最好形式，简洁严谨、去伪存真、自动运行。我们研发的知识代数将梳理各个知识库的关系，为学习者规划路径，做到不重不漏，能大大提高学习效率，从而能够攀越更高科技高峰。我们研发的科教领域计算库都将开源，为教育、生产服务。我正在筹办零空科教基金会，支持我们的开源软件以及在教育领域的研发和应用。
	</p>


</section>


